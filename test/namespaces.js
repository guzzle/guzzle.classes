var assert = require("assert");
require("../");

describe('Namespaces', function(){
    describe('Check guzzle namespace', function(){

        it('Defined global object Guzzle', function(){
            assert.ok(Guzzle !== undefined);
        });

        it('Defined Guzzle.ns and Guzzle.ns has function', function(){
            assert.ok(Guzzle.ns !== undefined);
            assert.ok( Guzzle.ns instanceof Function);
        });


        it('Create new namespace with root and nested spaces', function(){

            Guzzle.ns('MyTestApp.Test.Deep.Mip');
            assert.ok(MyTestApp.Test.Deep.Mip !== undefined);

        });
    });

    describe('Create user object in custom namespace', function(){

        it('Create namespace with last object', function(){
            Guzzle.ns('MyTestApp.Test.IncludeObject', function() { return 'OK'; });
            assert.ok(MyTestApp.Test.IncludeObject instanceof Function);
        });

        it('Achtung!!! Guzzle.ns doesn\'t distinguish functions and objects', function(){
            Guzzle.ns('MyTestApp.Test.IncludeObject.OK', function() { return 'OK'; });
            assert.ok(MyTestApp.Test.IncludeObject instanceof Function);
            assert.ok(MyTestApp.Test.IncludeObject.OK instanceof Function);
            assert.equal(MyTestApp.Test.IncludeObject.OK(), MyTestApp.Test.IncludeObject());
        });

        it('Achtung!!! Guzzle.ns dont rewrite existing namespace', function(){
            var newFn = function() { return 'NOOK'; };
            assert.notEqual(newFn, Guzzle.ns('MyTestApp.Test.IncludeObject', newFn));
            assert.ok(MyTestApp.Test.IncludeObject() === 'OK');
        });

    });
});
