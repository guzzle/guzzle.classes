var ns = function(ns, lastObject) {
    var obj = global, i = 0, path = ns.split("."), lo = lastObject || {};
    for (;i<path.length;i++) {
        obj[path[i]]=obj[path[i]] || (i == path.length-1 ? lo : {}) ;
        obj=obj[path[i]];
    }
    return obj;
};

var getClass = function(className) {

    var chunks, i = 1, ns;
    console.log(className);
    if (className instanceof Object) {
        return className;
    }

    if (typeof className != 'string' || !className.length) {
        throw new Error('[Guzzle.getClass] Invalid class name.');
    }

    chunks = className.split('.');
    ns = global[chunks[0]]

    for(i;i<chunks.length;i++) {
        ns = ns[chunks[i]];
        if (ns === undefined || !(ns instanceof Object)) {
            throw new Error('[Guzzle.getClass] Class ('+className+') not initialised.');
        }
    }

    if (ns instanceof Function) {
        return ns;
    } else {
        throw new Error('[Guzzle.getClass] '+className+' is not constructor.');
    }

}


ns('Guzzle.Manager');
ns('Guzzle.utils');

Guzzle.ns = ns;
Guzzle.getClass = getClass;

ns('Guzzle.Manager.Classes', (new function() {

    var classes = {};

    this.define = function(className, data) {

        var constr, privates, parent, cls = function() {}, extend = cls;

        if (typeof className != 'string' || !className.length) {
            throw new Error('[Guzzle.define] Invalid class name.');
        }

        privates = data.privates || {};
        delete data.privates;
        with (privates) {
            constr = function() {};
        }


        if (data.extend) {
            extend = data.extend;
            delete data.extend;
        }

        parent                       = Guzzle.getClass(extend);
        cls.prototype                = parent.prototype;
        constr.prototype             = new cls();
        constr.prototype.constructor = constr;
        data.$className              = className;

        Guzzle.ns(className, constr);

        return constr;
    }//,


    //this.addMembers = function()

}()));

Guzzle.define = Guzzle.Manager.Classes.define;